# Roadmaps

Como uma das premissas do grupo é a democratização do conhecimento sobre hacking, estão em processo de desenvolvimento alguns Roadmaps: Arquivos que registram alguns dos tópicos e recursos mais pertinentes em cada uma das áreas para facilitar o ingresso a área de InfoSec.

Acrescentamos alguns desafios de Capture The Flag para aprender e colocar em prática os conhecimentos dos tópicos abordados.

**🚨🚨🚨 Esse roadmap é totalmente baseado nas nossas experiências como um grupo.**

## [Básico](./basico.md)

O documento [`basico.md`](./basico.md) contém as informações necessárias para iniciar. Nele há recursos externos e informações, básicas e superficiais, sobre os seguintes tópicos:

- Linux
- Criptografia/Hashes
- Redes
- Web
- OSINT
- Scripting
- Computação Forense

